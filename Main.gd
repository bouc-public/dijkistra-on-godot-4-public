extends AspectRatioContainer

@onready var _pm = get_node("VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer2/MenuBar/Start_from")
@onready var _pm_a = get_node("VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer2/MenuBar2/Connect_to")
var graph = {}
var graph_key = []
var file_graph_path


###   ----------------------- Path Node List   ---------------------------------   ###
var ElencoNodi = "VBoxContainer/HBoxContainer2/VBoxContainer2/ElencoNodi"
var InsertNode = "VBoxContainer/HBoxContainer2/VBoxContainer/InsertNode"
var LabelNodoArrivo = "VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer/LabelNodoArrivo"
var LabelNodoPartenza = "VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer/LabelNodoPartenza"
var InsertDistance = "VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer3/InsertDistance"
var InsertTime = "VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer3/InsertTime"
var CaricaFile = "VBoxContainer/HBoxContainer2/VBoxContainer/CaricaFile"
var SalvaFile = "VBoxContainer/HBoxContainer2/VBoxContainer/SalvaFile"
var StartDijk = "VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/LineEditStartNode"
var FinishDijk = "VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer2/LineEditFinishNode"
var RichTextDist = "VBoxContainer/VBoxContainer/HBoxContainer/RichTextDist"
var LineEditDistNum = "VBoxContainer/VBoxContainer/HBoxContainer/LineEditDistNum"
var RichTextTime = "VBoxContainer/VBoxContainer/HBoxContainer2/RichTextTime"
var LineEditTimeNum = "VBoxContainer/VBoxContainer/HBoxContainer2/LineEditTimeNum"
var RichTextLabelMessages = "VBoxContainer/HBoxContainer2/VBoxContainer2/RichTextLabelMessages"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass





func update_node_list():
	get_node(ElencoNodi).clear()
	for node in graph:
		var nome_nodo = (node + ": ")
		var connessioni_nodo = str(graph[node])
		get_node(ElencoNodi).newline()
		get_node(ElencoNodi).append_text(nome_nodo + connessioni_nodo)






func _on_insert_node_button_pressed():
	if get_node(InsertNode).text :
		var new_node = get_node(InsertNode).text
		if not graph.has(new_node):
			graph[new_node] = {}
			graph_key.append(new_node)
			_pm.add_item(new_node)
			_pm_a.add_item(new_node)
	update_node_list()


func _on_add_connection_pressed():
	if get_node(LabelNodoArrivo).text:
		if get_node(LabelNodoPartenza).text:
			if get_node(LabelNodoArrivo).text != get_node(LabelNodoPartenza).text:
				if int(get_node(InsertDistance).text):
					if int(get_node(InsertTime).text):
						var n1 = get_node(LabelNodoArrivo).text
						var n2 = get_node(LabelNodoPartenza).text
						var node_distance = int(get_node(InsertDistance).text)
						var node_time = int(get_node(InsertTime).text)
						graph[n1][n2] = [node_distance, node_time]
						graph[n2][n1] = [node_distance, node_time]
					else: get_node(RichTextLabelMessages).text = "Please, insert time."
				else: get_node(RichTextLabelMessages).text = "Please, insert distance."
			else: get_node(RichTextLabelMessages).text = "Start node and destination node must be different"
		else: get_node(RichTextLabelMessages).text = "Please, insert start node."
	else: get_node(RichTextLabelMessages).text = "Please, insert destination node."
	update_node_list()





func _on_start_from_index_pressed(index):
	get_node(LabelNodoPartenza).text = graph_key[index]

func _on_connect_to_index_pressed(index):
	get_node(LabelNodoArrivo).text = graph_key[index]

func _on_clear_pressed():
	get_node(LabelNodoPartenza).text = ""
	get_node(LabelNodoArrivo).text = ""
	get_node(InsertDistance).text = ""
	get_node(InsertTime).text = ""





### --------------------   load file   ---------------------------- ###
func _on_apri_file_pressed():
	get_node(CaricaFile).visible = true


func _on_carica_file_file_selected(path):
	var file = FileAccess.open(path, FileAccess.READ)
	var graph_text = file.get_as_text()
	graph = JSON.parse_string(graph_text)
	
	for k in graph:
		graph_key.append(k)
		_pm.add_item(k)
		_pm_a.add_item(k)
	
	update_node_list()





### ----------------------   save file   ---------------------------- ###
func _on_salva_file_button_pressed():
	get_node(SalvaFile).visible = true


func _on_salva_file_file_selected(path):
	var file = FileAccess.open(path, FileAccess.WRITE)
	file.store_string(str(graph))




func _on_button_dijkistra_pressed():
	if get_node(StartDijk).text and get_node(StartDijk).text in graph:
		if get_node(FinishDijk).text and get_node(FinishDijk).text in graph:
			if get_node(FinishDijk).text != get_node(StartDijk).text:
				var route_dist = dijkistra(graph, get_node(StartDijk).text, get_node(FinishDijk).text, 0)
				var route_time = dijkistra(graph, get_node(StartDijk).text, get_node(FinishDijk).text, 1)
				get_node(RichTextDist).text = str(route_dist[0])
				get_node(LineEditDistNum).text = str(route_dist[1])
				get_node(RichTextTime).text = str(route_time[0])
				get_node(LineEditTimeNum).text = str(route_time[1])
			else: get_node(RichTextLabelMessages).text = "Start city and destination must be different."
		else: get_node(RichTextLabelMessages).text = "Please, enter a correct destination."
	else: get_node(RichTextLabelMessages).text = "Please enter a correct city to start from."







###   -----------------------------------------------------------------------------------------------   ###
###   ------------------------------------   DIJKISTRA ALGORITHM   ----------------------------------   ###
###   -----------------------------------------------------------------------------------------------   ###
func dijkistra(graph, source, destination, mode):
	var inf = 99000000 #used as "infinite"
	var node_data = {}
	for k in graph: #set dictionary with city, cost (infinite) and predecessor in routing (initially empty)
		node_data[k] = {"cost": inf, "pred": []}
	node_data[source]["cost"] = 0
	var visited = []
	var temp = source #city analyzed in the for cycle
	for i in graph:
		if temp not in visited:
			visited.append(temp)
			for j in graph[temp]: #try all connection of the city (if not already visited)
				if j not in visited:
					var cost = node_data[temp]["cost"] + graph[temp][j][mode] #"temp" city cost + new connection cost 
					if cost < node_data[j]["cost"]: #if lesser of old cost, set in node_data as new cost
						node_data[j]["cost"] = cost
						node_data[j]["pred"] = temp
		temp = get_minimum_cost(node_data, visited) #the new "temp" city will be the non visited one with minimum cost
		if temp == destination:#end, best route find
			break
	var best_route = best_route_func(node_data, source, destination)
	return best_route

func get_minimum_cost(node, visited):
	var cost = 99000000
	var minimum_cost
	for k in node:
		if k not in visited:
			if node[k]["cost"] < cost:
				cost = node[k]["cost"]
				minimum_cost = k
	return minimum_cost

func best_route_func(node, sour, dest):
	var route = []
	var cost = node[dest]["cost"]
	var temp = node[dest]["pred"] #from destination, go back to source
	while temp != sour:
		route.append(temp)
		temp = node[temp]["pred"]
	route.append(temp)
	route.reverse() #reverse to start from source
	route.append(dest)
	return [route, cost]
